var _       = require('lodash');
var request = require('request');
const async = require('async');


// NOTE: Global varaibles to be used with async.series chaining function
var TOKEN = 'token feed9c4bbe47e6dbfa0ae55ae703d716290e5d10'  // Limited authorization account
var ALL_USERS = []
var ALL_USERS_COUNT = 0
var ALL_USERS_Ratings = []



// In case the user did not pass any command line params, a default value is created in getCityDevelopers function (set to Berlin)
var city = process.argv[2]


// Chaining Tasks to be visually readable/traceable
async.series(
	[
		(callBack) => {getCityDevelopers(city , callBack)},
		(callBack) => {getDeveloperRepos(callBack)},
		(callBack) => {sortUserRatings(callBack)},
	],
	(err)=>{
		if (err) {
			console.log("This error occured: ",err);
		}
		console.log("\Done successfully!\n");

		// console.log(ALL_USERS_Ratings);

		console.log("===================================");
		console.log("Highest rated JavaScript developers:");
		console.log("===================================");
		ALL_USERS_Ratings.slice(-3).reverse().forEach((user)=>{
			console.log("Name: " , user.username , ' ==> ' , 'Rating: ' , user.ratings);
		})
	}
)



// using function default values in case user passed an empty cmd parameter
function getCityDevelopers( city='Berlin' , callBack ) {

	// example:: https://api.github.com/search/users?q=type:user+language:javascript+location:Cairo&sort=score&order=desc

	var type	 = 'user'
	var language = 'javascript'

	var URL = "https://api.github.com/search/users?q="
	var query_url = URL + "type:" + type + '+language:' + language + '+location:' + city + '&per_page=100' // 100 items per page is the maximum

	request({
		url : query_url,
		method : "get",
		headers : {
			'User-Agent' : 'cmd',
			'Authorization': TOKEN
		}
		},
		(error,response,body) =>{

			var github_json_users = JSON.parse(body);

			ALL_USERS_COUNT = github_json_users['total_count']

			console.log('Total Number of users: ',ALL_USERS_COUNT);

			ALL_USERS = github_json_users['items']

			// Caculater the number of pages we need to go through to collect all the users
			var number_of_pages = _.ceil( ALL_USERS_COUNT/100 )

			console.log('number_of_pages',number_of_pages);

			// maximum number of search items returned equals 1000 entry, which means 10 pages of 100 entries
			number_of_pages =number_of_pages > 10 ? 10: number_of_pages

			// Paginate all the available pages, with concurrency of 1 to properly collect users
			var page_index_range = _.range(2 , number_of_pages+1)

			// load users of one page at a time to visuall view them and avoid failure
			async.forEachLimit( page_index_range , 1 , (index,cb)=>{

				console.log('page index: ',index);

				var URL = "https://api.github.com/search/users?q="
				var query_url = URL + "type:" + type + '+language:' + language + '+location:' + city + '&per_page=100' + '&page=' + index

				request({
					url : query_url,
					method : "get",
					headers : {
						'User-Agent' : 'cmd',
						'Authorization': TOKEN
					}
					},
					(error,response,body) =>{

						var github_json_users = JSON.parse(body);

						// extending the array
						ALL_USERS.push.apply(ALL_USERS , github_json_users['items'])

						console.log('Aggregate collected users: ', index, ' ' , ALL_USERS.length);

						cb()
					}) // end of inner request

			},(err)=>{
				if (err) {
					console.log("Error collecting all users from all pages");
				}
				console.log("\nCollected successfully all the users form all the pages");

				console.log("\nThis may take a few to several minutes to collect each user's repos, adding each repo stars, and sorting all the users\n");

				console.log("LOADING ....\n");

				callBack() // callBack of async.series

			}) // end of async.forEachLimit

		}) // end of outer request
}



function getDeveloperRepos(callBack) {

	//example:: https://api.github.com/users/Benoitlecorre/repos

	var URL = "https://api.github.com/users/"

	// Maximum allow 20 concurrent connections only, that's to prevent explosion of event-loop
	async.forEachLimit(ALL_USERS , 10, (user,cb)=>{

		var username = user['login']

		var query_url = URL + username + '/repos'

		var total_starts_count = 0

		request({
		          url: query_url,
		          method: 'get',
				  headers : {
					  'User-Agent' : 'cmd',
					  'Authorization': TOKEN
			  	  }
		      }, function(error, response, body){
		          if(error) {
		              return console.error('Error: ' , error);
		          } else {
		            //   console.log(response.statusCode, body);
					var user_repos = JSON.parse(body)

					// Initial aggregate rating value per user
					var user_aggregate_ratings = 0

					user_repos.forEach((repo)=>{
						user_aggregate_ratings += repo['stargazers_count']
					})

					ALL_USERS_Ratings.push({username:username , ratings:user_aggregate_ratings})
					cb()  // the final callback for forEach

				} // end of else
	      })
	}, (err)=>{
		if (err) {
			console.log("Error in async.forEach: ",err);
		}
		callBack() // the callback needed by async.series
	}) // end of async.forEachLimit
}


function sortUserRatings(callBack) {

	// binary sort for the users aggregate ratings
	ALL_USERS_Ratings.sort( (a,b) => {return a.ratings - b.ratings} )

	callBack()
}
