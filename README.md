Things that worth to be noted about Github API:
===============================================
1. The maximum number of items that can be retrieved in one request (one page) are 100 elements
2. The maximum number of aggregate items that Github's API allow for authenticated users to retreive is 1000 elements (10 pages total, where each page has 100 elements)
3- We will NOT abe able to retrieve more that 1000 elements, event if the search results are more than that

-------------------------------------

# General notes about the application:


Async library has been employed to enable different types of workflows, and control the concurrency of requests that hit the Github API at some points of the application.

## Main functions used:
1. getCityDevelopers: Collects all JavaScript Develoeprs in a specific city, if no city is passed as a parameter in the command line, Berlin will be used as a default city.
2. getDeveloperRepos: Collects each user javaScript repos, and sum up each repo stars
3. sortUserRatings: Sorts the collected users

## Workflows used:

1. async.series  => Allows the specified functions to be run in series (as a replacement to callback hell that node.js is konwn for)
2. async.forEach => Process each item (they are run in parallel) with a custom task/function, and when all the elements are processed, do something else
3. async.forEachLimit => Same as above, but controls concurrency

## Todos

1. Adding input sanitization using regex or similar
2. In case of computationally intesive operations, we may fork other processes instead of only using async
3. A web interface to collect users inputs, and show results
4. Unit Testing